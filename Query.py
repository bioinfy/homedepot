# CS-223: Bioinformatics
# Dr. Wesley
# File Name:  Query.py 
# Project: Kaggle Home Depot Search Relevance
# By: Joe James, Aneri Chavda, Tejas Saoji, Lingfang Gao, Rachana Desai 
# Date: May 12, 2016 
# Python Version(s) 2.7.3: 
""" 
FILE DESCRIPTION: 
    The Query file contains the Query class and all its getters and setters.
    This file depends upon the SuperQuery module, which is its super-class.

MODIFICATION HISTORY:     
    4/20/2016, added SuperQuery and inheritance, Rachana Desai and Joe James
    4/19/2016, added score instance variables for attrs, desc, title, brand, Ling Gao
    4/12/2016, created Query class, Joe James, Aneri Chavda, Tejas Saoji, Lingfang Gao, Rachana Desai 
"""
    
from SuperQuery import SuperQuery
    
class Query(SuperQuery):
    """The Query class stores all information for each train or test query, 
    including query id, product id, search keywords, and estimated scores
    (and actual score for training queries only).
    It has setter and getter functions for each of these instance attributes."""
    
    classAttribute = ('Mean AttributeScore Error = x', 'Mean BrandScore Error = x', 
        'Mean DescriptionScore Error = x', 'Mean TitleScore Error = x')     # Tuple example, and Class Attribute
    
    def __init__(self, qid, pid):
        SuperQuery.__init__(self)
        self.queryId = qid
        self.prodId = pid
        self.keywords = ''
        self.keywordsList = []
        self.actualScore = None # actual score, only for training set
        self.attrScore = None    # our estimated score based on attributes
        self.brandScore = None
        self.descScore = None
        self.titleScore = None
    
    def get_prodId(self):
        return self.prodId
            
    def set_keywords(self, kw = None):  # default argument value
        self.keywords = kw
        if kw is not None:
            self.keywordsList = kw.split()
    def get_keywords(self):
        return self.keywords
    def get_keywordsList(self):
        return self.keywordsList
        
    def set_actualScore(self, score):   # for training set only
        self.actualScore = score
    def get_actualScore(self):
        return self.actualScore
        
    def set_attrScore(self, score):
        self.attrScore = score
    def get_attrScore(self):
        return self.attrScore
        
    def set_brandScore(self, score):
        self.brandScore = score
    def get_brandScore(self):
        return self.brandScore
        
    def set_descScore(self, score):
        self.descScore = score
    def get_descScore(self):
        return self.descScore
        
    def set_titleScore(self, score):
        self.titleScore = score
    def get_titleScore(self):
        return self.titleScore   
        
    def get_attrScoreDelta(self):   # how far off our estimated score is from the actual score, for training set only
        try:
            return abs(self.get_actualScore() - self.get_attrScore())
        except:
            return None
    
    