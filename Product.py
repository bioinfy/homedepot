# CS-223: Bioinformatics
# Dr. Wesley
# File Name:  Product.py 
# Project: Kaggle Home Depot Search Relevance
# By: Joe James, Aneri Chavda, Tejas Saoji, Lingfang Gao, Rachana Desai 
# Date: May 12, 2016 
# Python Version(s) 2.7.3: 
""" 
FILE DESCRIPTION: 
    The Product file contains the Product class and all its getter and setter
    functions. This file does not depend on any other modules.

MODIFICATION HISTORY:     
    4/19/2016, changed description to a dictionary of words with counts, Tejas Saoji
    4/17/2016, added brand attribute, Aneri Chavda
    4/12/2016, created Product class, Joe James, Aneri Chavda, Tejas Saoji, Lingfang Gao, Rachana Desai 
"""

class Product:
    """
    The Product class stores all information for each product, 
    including product id, attributes, brand, description, and title.
    It has setter and getter functions for each of these instance attributes.
    """
    def __init__(self, id):
        self.prodId = id
        self.attributes = {}
        self.description_words_list = {}
        self.title = ''
        self.brand = ''
        
    def get_prodId(self):
        return self.prodId

    def add_attribute(self, key, value):
        self.attributes[key] = value
        if key == 'mfg brand name':
            self.set_brand(value)
    def get_attributes(self):
        return self.attributes
    def get_attributes_string(self):
        str = ''
        for k, v in self.attributes.items():
            str += k + ' ' + v + ' '
        return str
        
    def increment_description_word_count(self, key, count):
        if key in self.description_words_list:
            self.description_words_list[key] = int(self.description_words_list[key]) + count
        else:
            self.description_words_list[key] = count
    def get_description_word_count(self, key):
        return self.description_words_list.get(key, "")
        
    def is_present_in_description(self, key):
        return key in self.description_words_list  
        
    def set_title(self, t):
        self.title = t
    def get_title(self):
        return self.title
        
    def set_brand(self, b):
        self.brand = b
    def get_brand(self):
        return self.brand

