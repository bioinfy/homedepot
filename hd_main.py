# CS-223: Bioinformatics
# Dr. Wesley
# File Name:  hd_main.py 
# Project: Kaggle Home Depot Search Relevance
# By: Joe James, Aneri Chavda, Tejas Saoji, Lingfang Gao, Rachana Desai 
# Date: May 12, 2016 
# Python Version(s) 2.7.3: 
""" 
FILE DESCRIPTION: 
    This file contains the main function for the project, as well as data loading
    functions, load_descriptions, load_attributes, load_queries. It also contains
    functions to compute scores, estimate_scores, determine accuracy of our estimated
    scores vs actual training scores, check_train_set_scores, and writes estimated
    scores for the test set to a file, save_test_scores. 
    This file also includes some helper functions to handle peripheral tasks like
    stripping punctuation and correcting spelling.
    This file depends upon the Product, Query, spell_check, csv, re, string,
    time, and nltk.stem.porter modules. 

MODIFICATION HISTORY:
    4/22/2016, added estimate_scores and save_test_scores functions, Tejas Saoji, Aneri Chavda
    4/20/2016, added documentation, Joe James, Rachana Desai, Ling Gao
    4/19/2016, added check train_set_scores function, Joe James, Rachana Desai, Ling Gao
    4/17/2016, added load_attributes function, Tejas Saoji, Aneri Chavda
    4/16/2016, added load_descriptions function, Tejas Saoji, Aneri Chavda
    4/14/2016, added load_queries function, Ling Gao, Joe James, Rachana Desai     
    4/12/2016, created hd_main file, Joe James, Aneri Chavda, Tejas Saoji, Lingfang Gao, Rachana Desai 
"""

import Product as pp
import Query as qq
import spell_check as sc
import csv
import re
import string
import time

#from nltk.stem.porter import *

def load_descriptions(products, filename):
    #TODO: write function to load the product descriptions
    f= open(filename)
    reader = csv.reader(f, delimiter = ',')
 #   stemmer = PorterStemmer()
    next(reader)
   
    for row in reader:
        words = []
        
        pid = int(row[0])
        line = row[1].strip()
        # line = removePunctuation(line)

        words = re.split('\\s+',line)
        
        for word in words:
            stemmedWord = word
            #str(stemmer.stem(word))
            if pid in products:
                products[pid].increment_description_word_count(stemmedWord, 1)
            else:
                p = pp.Product(pid)
                p.increment_description_word_count(stemmedWord, 1)
                products[pid] = p 
        
       
        #for word in products[pid].description_words_list:
        #    print word + " :  " + str(products[pid].get_description_word_count(word))
       
    #print " length :  " + str(len(products[100001].description_words_list))
    #print " length :  " + str(len(products[100002].description_words_list))
    #print " length :  " + str(products[100002].is_present_in_description('sldfjbucewi'))
    #print "\n\n\nlength of products list :: " + str(len(products))
    f.close()                               
    return products    

def load_attributes(products, filename):
    # A function to load the product attributes
    f = open(filename)
    reader = csv.reader(f, delimiter = ',')
    next(reader)
    
    for row in reader:
        if len(row[0]) > 2:
            pid = int(row[0])
            attribute_name = row[1].lower()
            attribute_value = row[2].lower()

            if pid in products:
                products[pid].add_attribute(attribute_name, attribute_value)
            else:
                p = pp.Product(pid)
                p.add_attribute(attribute_name, attribute_value)
                products[pid] = p 

    #print " attr string ::\n  " + str(products[100001].get_attributes().get('Material'))
    #print " length :  " + str(len(products[100002].description_words_list))
    #print " length :  " + str(products[100002].is_present_in_description('sldfjbucewi'))
    #print "\n\n\nlength of products list :: " + str(len(products))
    f.close()                            
    return products 

def check_spelling(queries):  
    for id, q in queries.items():
        kws=''
        for w in q.get_keywordsList():
            kws+=sc.spell_check(w)+' '
        kws=kws.strip() #remove the end space
        q.set_keywords(kws)  
    
'''    
DESCRIPTION:
    <load_queries> reads through each line in train/test csv file, fetches the qid and 
    pid from the file, creates a new Query object, and adds it to the queries dict. 
    It also gets the keyword from train.csv file and sets the keyword 
    to query object, then it checks for the length of the line if it is more than 4, 
    it assign the floating value of score to the variable score and sets the score 
    to the Query object, (for training set only). Next it creates 
    the dictionary of queries with the key of query id and its query object. 
    Also, it gets the title from the query file, 
    if the product id exists in the products dict it sets the title for that product id,
    and if the product id is not found then it creates a new Product object, and sets the title for it.  
PRECONDITIONS: 
    products dictionary must be passed in as an argument.
    Some of product ids might not be there, so we can create new product id and 
    assign new title to the new product id. 
POSTCONDITIONS: 
     Every query id should have Query object in the queries dict, and every 
     product id in the train/test csv file should have a Product object with title 
SIDE EFFECTS: 
    There are no side effects; the function only updates the titles of the products dict,
    and populates the queries dict with Query objects read from the csv file.
MODIFICATION HISTORY:
    4/22/2016, added documentation, Tejas Saoji, Aneri Chavda 
    4/15/2016, revised function to support both test and training set queries, Ling Gao,Joe James, Rachana Desai     
    4/12/2016, created load_queries function, Joe James, Aneri Chavda, Tejas Saoji, Lingfang Gao, Rachana

'''
def load_queries(products, queries, filename):
    queries = {}
    f = open(filename)
    reader = csv.reader(f, delimiter = ',')
    next(reader)
    for line in reader:
        qid = int(line[0])
        pid = int(line[1])
        q = qq.Query(qid,  pid)
        
        kw = line[3].lower()
        q.set_keywords(kw)
        if len(line) > 4:
            score = float(line[4])
            q.set_actualScore(score)
        queries[qid] = q
        
        title = line[2].lower()
        if pid in products:
            products[pid].set_title(title)
        else:
            p = pp.Product(pid)
            p.set_title(title)
            products[pid] = p 
            
    f.close()   
    #check_spelling(queries)   
    return products, queries
    
#TODO need to modify this function to make it generic for test and train both queries    
#Estimate score for every query    
def train_model(products, queries):
    #TODO: write algorithm to compute est scores for all Queries in a query set
    # set attrScore, brandScore, descScore and titleScore for each Query
    
    weightage = {'description':25.0,'title':25.0,'attributes':25.0, 'brand':25.0}
    count = {'description':0.0,'title':0.0,'attributes':0.0, 'brand':0.0}
    total = 0
    for queryId in queries:
        
        query = queries[queryId]
        product = products[query.get_prodId()]
        descriptionScore, titleScore, attributeScore, brandScore = calculate_intermediate_scores(query, product)
        #If training data

        actualScore = query.get_actualScore()
        descriptionSkew = float(abs(actualScore - descriptionScore))
        titleSkew = float(abs(actualScore - titleScore))
        attributeSkew = float(abs(actualScore - attributeScore))
        brandSkew = float(abs(actualScore - brandScore))
            
        minSkew = min(descriptionSkew,titleSkew, attributeSkew, brandSkew)
            
        if(descriptionSkew == minSkew):
            count['description'] += 1             
            total += 1
        if(titleSkew == minSkew):
            count['title'] += 1             
            total += 1
        if(attributeSkew == minSkew):
            count['attributes'] += 1             
            total += 1
        if(brandSkew == minSkew):
            count['brand'] += 1             
            total += 1            
    

    weightage['description'] = (count['description'] / total) 
    weightage['title'] = (count['title'] / total ) 
    weightage['attributes'] = (count['attributes'] / total ) 
    weightage['brand'] = (count['brand'] / total ) 
    print count
    print weightage
    return queries, weightage
    pass
    
   
def test_model(products, queries, weightage):
    
    for queryId in queries:
        
        query = queries[queryId]
        product = products[query.get_prodId()]
        descriptionScore, titleScore, attributeScore, brandScore = calculate_intermediate_scores(query, product)
        computedScore = float ((descriptionScore * weightage['description']) + (titleScore * weightage['title']) + (attributeScore * weightage['attributes']) + (brandScore * weightage['brand'] ) )

        print computedScore
        query.set_actualScore(computedScore)
    
    return queries    
    pass

def calculate_intermediate_scores(query, product):
    descriptionMatchingWords = 0
    titleMatchingWords = 0
    attributeMatchingWords = 0
    brandMatchingWords = 0

    for word in query.get_keywordsList():

        if product.is_present_in_description(word):
            descriptionMatchingWords += 1
        if word in product.get_title():
            titleMatchingWords += 1
        if word in product.get_attributes_string():
            attributeMatchingWords += 1
        if product.get_brand() and word in product.get_brand().split():
            brandMatchingWords += 1
                                        
    totalWords = len(query.get_keywordsList())                       
        
    #Normalizes the score in the range of 1.0 to 3.0                 
    descriptionScore = (float(descriptionMatchingWords)/float(totalWords) * 2.0 ) + 1.0
    titleScore = ( float(titleMatchingWords)/float(totalWords) * 2.0) + 1.0
    attributeScore = ( float(attributeMatchingWords)/float(totalWords) * 2.0 ) + 1.0
    brandScore = 1.0
    if product.get_brand(): 
        brandScore = (float(brandMatchingWords)/float(len(product.get_brand().split())) * 2.0 ) + 1.0


    query.set_descScore(descriptionScore)
    query.set_titleScore(titleScore)
    query.set_attrScore(attributeScore)
    query.set_brandScore(brandScore)
    
    return descriptionScore, titleScore, attributeScore, brandScore
        
          
def check_train_set_scores(train):
    #TODO: determine accuracy of training set actual vs estimate scores
    # for attrScore, brandScore, descScore and titleScore for each Query, and print mean error
    attrSumCount = [0.0, 0] # [sum_of_error, count]
    brandSumCount = [0.0, 0]
    descSumCount = [0.0, 0]
    titleSumCount = [0.0, 0]
    for q in train.values():
        if q.get_attrScore():
            attrSumCount[0] += abs(q.get_actualScore() - q.get_attrScore())
            attrSumCount[1] += 1
        if q.get_brandScore():
            brandSumCount[0] += abs(q.get_actualScore() - q.get_brandScore())
            brandSumCount[1] += 1
        if q.get_descScore():
            descSumCount[0] += abs(q.get_actualScore() - q.get_descScore())
            descSumCount[1] += 1
        if q.get_titleScore():
            titleSumCount[0] += abs(q.get_actualScore() - q.get_titleScore())
            titleSumCount[1] += 1
            
        if attrSumCount[1] > 0:
            print q.classAttribute[0][:-1] + str(attrSumCount[0] / attrSumCount[1])     # Slice example, and uses Tuple
        if brandSumCount[1] > 0:
            print q.classAttribute[1][:-1] + str(brandSumCount[0] / brandSumCount[1])
        if descSumCount[1] > 0:
            print q.classAttribute[2][:-1] + str(descSumCount[0] / descSumCount[1])
        if titleSumCount[1] > 0:
            print q.classAttribute[3][:-1] + str(titleSumCount[0] / titleSumCount[1])
            
    return train
    
def save_test_scores(test_queries):

    with open('output.csv', 'w') as csvfile:
        fieldnames = ['Query Id', 'Estimated Score']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter=',', lineterminator='\n')
        writer.writeheader()        
    
        listOfKeys = test_queries.keys()
        print ('{0:^15s} | {1:^15s}'.format('Query Id','Estimated Score'))
        
        for key in listOfKeys:
            print ('{0:^15d} | {1:^13.2f}'.format(key,test_queries.get(key).get_actualScore()))
            estimatedScore = test_queries.get(key).get_actualScore()
            writer.writerow({'Query Id': key, 'Estimated Score': estimatedScore})



def is_punct_char(char):
    # check if char is punctuation char
    if char in string.punctuation:
         return True
    else:
         return False

""" 
DESCRIPTION:
    <removePunctuation> process the line by remove all the punctuation 
    except the demical point of the description text in "product_descriptions.csv" 
    file. Then we can process line by split the line using space and save them 
    into a list. 
PRECONDITIONS: 
    Before method <removePunctuation> is called, line is filled with words and 
    punctuation. Open and read the cvs file by delimiter "," line by line, 
    locate description in line[1], and call strip() to remove the space at the 
    end of each line.
POSTCONDITIONS: 
     Punctuation in the line should be replaced by while space, except the 
     demical point.
SIDEEFFECTS: 
    For some special case, period might be thought as a puntuation. eg. "dig period 
    digit demical point digit", in this case, the function could not tell which 
    one is punctuation, and which one is demical point.
MODIFICATION HISTORY:
    4/22/2016, added documentation, Ling Gao,Joe James, Rachana Desai
    4/16/2016, added more marks, Tejas Saoji, Aneri Chavda     
    4/14/2016, created function, Joe James, Aneri Chavda, Tejas Saoji, Lingfang Gao, Rachana Desai

"""
def removePunctuation(str):
    str = str.lower().strip()
    myString = ""
    prevChar = ""
    nextChar = "" 
    idxOfChar = 0
    index = 0
    
    for c in str:
        idxOfChar = str.index(c, index)
        
        nextCharIdx = idxOfChar+1
        if(nextCharIdx < len(str)):
            nextChar = str[nextCharIdx]
        
        if is_punct_char(c)!= True:
            myString += c
        else:
            if (prevChar != "" and prevChar.isdigit()):
                if (nextChar != "" and nextChar.isdigit()):
                    myString += c
                else:
                    myString += ' '
            else:                
                myString += ' '
                
        prevChar = c
        index += 1    
    
    return myString 
    
# this runs 3 minutes faster than removePunctuation (for descriptions only)
# but not sure if it works as well. need to compare results of both functions
def removePunctuation2(s):
    return s.translate(string.maketrans("",""), string.punctuation)
    
# receives a string and returns a string with stop words stripped out
#def stripStopWords(query):
#    stop = stopwords.words('english')
#    queryTerms = query.split()
#    queryTerms = [word for word in queryTerms if word not in stop]
#    return ' '.join(queryTerms)
        
def main():
    start_time = time.time()
    products = {}
    train_queries = {}
    test_queries = {}
    products, train_queries = load_queries(products, train_queries, "train.csv")
    products = load_descriptions(products, "product_descriptions.csv")
    products = load_attributes(products, "attributes.csv")
    products, test_queries = load_queries(products, test_queries, 'test.csv')
    train_queries, weightage = train_model(products, train_queries)
    test_queries = test_model(products, test_queries, weightage)

    save_test_scores(test_queries)
    print 'Num Train Queries: ' + str(len(train_queries))
    print 'Num Test Queries: ' + str(len(test_queries))
    print 'Num Products: ' + str(len(products))
    finish_time = time.time()
    print "Run time (sec): " + str(int(finish_time - start_time))

main()