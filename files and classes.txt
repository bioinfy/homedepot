FILES
-----------------
product_descriptions.csv
    prodId (int)
    description (string)

attributes.csv
    prodId (int)
    attributes {key:value}
    
train.csv
    queryId (int)
    prodId (int)
    title (string)
    actualScore (float)
    
test.csv
    queryId (int)
    prodId (int)
    title (string)
    
=============================================
CLASSES
-------------------
Product
    prodId (int)
    attributes (dict)
    description (string)
    title (string)
    brand (string)
    
Query
    queryId
    prodId
    keywords (string)
    keywordsList (list of strings)
    actualScore
    estScore
    
spell_check.py
    not a class, just a function used to correct spelling in query words
    receives a word (string) and returns either the same word or a corrected word if it is mis-spelled
        drill -> drill
        grindre -> grinder
    in some cases returns a string with multiple words, 
        washingmachine -> 'washing machine'
        lithium-ion -> 'lithium ion'
    also strips off endings from some words,
        wndows -> window (singular version)