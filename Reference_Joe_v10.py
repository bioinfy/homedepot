# Home Depot
# dict products, prod_id => {'description' => desc, 'attributes' => attrs, 'title' => title}
# list queries, {'id' => query_id, 'keywords' => kwds, 'actual_score' => score}
# SCORE: 0.55763, submitted 3/15/2016
# SCORE: 0.54334, submitted 3/17/2016
# SCORE: 0.51864, submitted 3/20/2016 (.4118 by my own scoring)

import csv
import math
import re
import spell_check as sc
from nltk.stem.porter import *

# got most of this code from example here, https://www.kaggle.com/the1owl/home-depot-product-search-relevance/rf-mean-squared-error/code
# improves score by .0084, but takes 12 minutes to run.
def string_cleanup(s):
    stop_w = ['for', 'xbi', 'and', 'in', 'th','on','sku','with','what','from','that','less','er','ing']
    strNum = {'zero':0,'one':1,'two':2,'three':3,'four':4,'five':5,'six':6,'seven':7,'eight':8,'nine':9}
    # stemmer = PorterStemmer()
    
    s = re.sub(r"(\w)\.([A-Z])", r"\1 \2", s) #Split words with a.A
    s = s.lower()
    s = s.replace("&nbsp"," ")  # my addition to delete spaces
    s = s.replace("  "," ")
    s = s.replace(","," ") #could be number. should use regex to check for numbers
    s = s.replace("$"," ")
    s = s.replace("?"," ")
    s = s.replace(";"," ")  # my addition to delete semi
    s = s.replace("-"," ")
    s = s.replace("//","/")
    s = s.replace("..",".")
    s = s.replace(" / "," ")
    s = s.replace(" \\ "," ")
    s = s.replace("."," . ")
    s = re.sub('[()]', '', s)   # my addition to delete parens
    
    s = re.sub(r"(^\.|/)", r"", s)
    s = re.sub(r"(\.|/)$", r"", s)
    s = re.sub(r"([0-9])([a-z])", r"\1 \2", s)
    s = re.sub(r"([a-z])([0-9])", r"\1 \2", s)
    s = s.replace(" x "," xbi ")
    s = re.sub(r"([a-z])( *)\.( *)([a-z])", r"\1 \4", s)
    s = re.sub(r"([a-z])( *)/( *)([a-z])", r"\1 \4", s)
    s = s.replace("*"," xbi ")
    s = s.replace(" by "," xbi ")
    s = re.sub(r"([0-9])( *)\.( *)([0-9])", r"\1.\4", s)
    s = re.sub(r"([0-9]+)( *)(inches|inch|in|')\.?", r"\1in. ", s)
    s = re.sub(r"([0-9]+)( *)(foot|feet|ft|'')\.?", r"\1ft. ", s)
    s = re.sub(r"([0-9]+)( *)(pounds|pound|lbs|lb)\.?", r"\1lb. ", s)
    s = re.sub(r"([0-9]+)( *)(square|sq) ?\.?(feet|foot|ft)\.?", r"\1sq.ft. ", s)
    s = re.sub(r"([0-9]+)( *)(cubic|cu) ?\.?(feet|foot|ft)\.?", r"\1cu.ft. ", s)
    s = re.sub(r"([0-9]+)( *)(gallons|gallon|gal)\.?", r"\1gal. ", s)
    s = re.sub(r"([0-9]+)( *)(ounces|ounce|oz)\.?", r"\1oz. ", s)
    s = re.sub(r"([0-9]+)( *)(centimeters|cm)\.?", r"\1cm. ", s)
    s = re.sub(r"([0-9]+)( *)(milimeters|mm)\.?", r"\1mm. ", s)
    s = s.replace("°"," degrees ")
    s = re.sub(r"([0-9]+)( *)(degrees|degree)\.?", r"\1deg. ", s)
    s = s.replace(" v "," volts ")
    s = re.sub(r"([0-9]+)( *)(volts|volt)\.?", r"\1volt. ", s)
    s = re.sub(r"([0-9]+)( *)(watts|watt)\.?", r"\1watt. ", s)
    s = re.sub(r"([0-9]+)( *)(amperes|ampere|amps|amp)\.?", r"\1amp. ", s)
    s = s.replace("  "," ")
    s = s.replace(" . "," ")
    s = (" ").join([z for z in s.split(" ") if z not in stop_w])
    s = (" ").join([str(strNum[z]) if z in strNum else z for z in s.split(" ")])
    # s = (" ").join([stemmer.stem(z) for z in s.split(" ")])
    s = s.lower()
    return s
    
def string_cleanup_query(s):
    stop_w = ['for', 'xbi', 'and', 'in', 'th','on','sku','with','what','from','that','less','er','ing']
    strNum = {'zero':0,'one':1,'two':2,'three':3,'four':4,'five':5,'six':6,'seven':7,'eight':8,'nine':9}
    stemmer = PorterStemmer()
    
    s = re.sub(r"(\w)\.([A-Z])", r"\1 \2", s) #Split words with a.A
    s = s.lower()
    s = s.replace("&nbsp"," ")  # my addition to delete spaces
    s = s.replace("  "," ")
    s = s.replace(","," ") #could be number. should use regex to check for numbers
    s = s.replace("$"," ")
    s = s.replace("?"," ")
    s = s.replace(";"," ")  # my addition to delete semi
    s = s.replace("-"," ")
    s = s.replace("//","/")
    s = s.replace("..",".")
    s = s.replace(" / "," ")
    s = s.replace(" \\ "," ")
    s = s.replace("."," . ")
    s = re.sub('[()]', '', s)   # my addition to delete parens
    if s[-1] == 's':
        s = s[:-1]
    
    s = re.sub(r"(^\.|/)", r"", s)
    s = re.sub(r"(\.|/)$", r"", s)
    s = re.sub(r"([0-9])([a-z])", r"\1 \2", s)
    s = re.sub(r"([a-z])([0-9])", r"\1 \2", s)
    s = s.replace(" x "," xbi ")
    s = re.sub(r"([a-z])( *)\.( *)([a-z])", r"\1 \4", s)
    s = re.sub(r"([a-z])( *)/( *)([a-z])", r"\1 \4", s)
    s = s.replace("*"," xbi ")
    s = s.replace(" by "," xbi ")
    s = re.sub(r"([0-9])( *)\.( *)([0-9])", r"\1.\4", s)
    s = re.sub(r"([0-9]+)( *)(inches|inch|in|')\.?", r"\1in. ", s)
    s = re.sub(r"([0-9]+)( *)(foot|feet|ft|'')\.?", r"\1ft. ", s)
    s = re.sub(r"([0-9]+)( *)(pounds|pound|lbs|lb)\.?", r"\1lb. ", s)
    s = re.sub(r"([0-9]+)( *)(square|sq) ?\.?(feet|foot|ft)\.?", r"\1sq.ft. ", s)
    s = re.sub(r"([0-9]+)( *)(cubic|cu) ?\.?(feet|foot|ft)\.?", r"\1cu.ft. ", s)
    s = re.sub(r"([0-9]+)( *)(gallons|gallon|gal)\.?", r"\1gal. ", s)
    s = re.sub(r"([0-9]+)( *)(ounces|ounce|oz)\.?", r"\1oz. ", s)
    s = re.sub(r"([0-9]+)( *)(centimeters|cm)\.?", r"\1cm. ", s)
    s = re.sub(r"([0-9]+)( *)(milimeters|mm)\.?", r"\1mm. ", s)
    s = s.replace("°"," degrees ")
    s = re.sub(r"([0-9]+)( *)(degrees|degree)\.?", r"\1deg. ", s)
    s = s.replace(" v "," volts ")
    s = re.sub(r"([0-9]+)( *)(volts|volt)\.?", r"\1volt. ", s)
    s = re.sub(r"([0-9]+)( *)(watts|watt)\.?", r"\1watt. ", s)
    s = re.sub(r"([0-9]+)( *)(amperes|ampere|amps|amp)\.?", r"\1amp. ", s)
    s = s.replace("  "," ")
    s = s.replace(" . "," ")
    s = (" ").join([z for z in s.split(" ") if z not in stop_w])
    s = (" ").join([str(strNum[z]) if z in strNum else z for z in s.split(" ")])
    # s = (" ").join([stemmer.stem(z) for z in s.split(" ")])
    s = s.lower()
    return s

def load_descriptions (filename, products):
    word_counts = {}
    # with open (filename, encoding="ISO-8859-1") as fin:
    with open (filename, encoding='UTF-8') as fin:
        for line in fin:
            try:
                comma = line.find(',')
                prod_id = line[:comma].strip()
                desc = line[comma+1:].strip().strip('"')
                products[prod_id] = {}
                products[prod_id]['description'] = string_cleanup(desc)
                word_list = desc.split(' ')
                word_list = set(desc.split())
                for word in word_list:
                    if word in word_counts:
                        word_counts[word][0] += 1
                    else:
                        word_counts[word] = [1, 0, 0]   # [desc, attrs, title]
                 
            except:
                print("ERROR in products")
    return products, word_counts

def load_attributes (filename, products):
    # with open (filename, encoding="ISO-8859-1") as fin:
    f = open (filename, encoding='UTF-8')
    reader = csv.reader(f, delimiter=',')
    for line in reader:
        try:
            id = line[0].strip()
            attr_name = line[1].strip()
            attr_desc = line[2].strip()
            attrs = string_cleanup(attr_name + ' ' + attr_desc)
            
            if id in products:
                if 'attributes' in products[id]:
                    products[id]['attributes'] += ' ' + attrs
                else:
                    products[id]['attributes'] = attrs
            else:
                products[id] = {}
                products[id]['attributes'] = attrs
            
            # get brand
            if line[1].strip() == 'MFG Brand Name':
                products[id]['brand'] = string_cleanup(line[2])
            
            # get color -- Color Family, Color/Finish, Color, Color/Finish Family
            l = line[1].strip()
            if l == 'Color Family' or l == 'Color/Finish' or l == 'Color' or l == 'Color/Finish Family':
                if 'color' in products[id]:
                    products[id]['color'] = products[id]['color'] + string_cleanup(line[2])
                else:
                    products[id]['color'] = string_cleanup(line[2])
               
        except:
            print ("ERROR in attributes")
    return products
    
def load_titles (filename, products):
    fin = open(filename, encoding="ISO-8859-1")
    reader = csv.reader(fin, delimiter=',')
    for line in reader:
        prod_id = line[1]
        if prod_id not in products:
            products[prod_id] = {}
        products[prod_id]['title'] = string_cleanup(line[2])
            
    return products
    
def load_queries (filename, products, train=True):
    fin = open(filename, encoding="ISO-8859-1")
    reader = csv.reader(fin, delimiter=',')
    queries = []
    for line in reader:
        query = {}
        query['id'] = line[0]
        query['prod_id'] = line[1]
        
        dirty = string_cleanup_query(line[3]).split(' ')
        cleaned = ''
        for word in dirty:
            cleaned += sc.spell_check(word) + ' '
        query['keywords'] = cleaned.strip().split(' ')
        
        if train:
            query['actual_score'] = float(line[4].strip())
        queries.append(query)
    return queries
    
# receives 4 scores in a list and computes a weighted average score
# scores = [attrs, brand, descr, title, color]
def get_mean_score(scores):
    weights = [1.2, 3.1, 1.2, 1.3, 0.01]
    numerator = sum([scores[i] * weights[i] for i in range(len(scores)) if scores[i] > 0.0])
    denominator = sum([weights[i] for i in range(len(weights)) if scores[i] > 0.0])
    wtd_avg = float(numerator / denominator)
    if wtd_avg < 1.3:
        wtd_avg = 1.3
    return min(2.7, wtd_avg)
    
def get_mean_score2(scores):
    # can't seem to tweak this down any better than .4621 mean score
    wtd_avg = 1.3
    brand = scores[1]
    title = scores[3]
    descr = scores[2]
    color = scores[4]
    attrs = scores[0]
    
    if brand > 2.0 and title > 2.4 and color > 2.0 and descr > 2.0:
        wtd_ave = 2.9
    elif brand > 2:
        wtd_avg = brand
    elif title > 2.0:
        wtd_avg = title
    elif descr > 2.0:
        wtd_avg = descr
    elif title > 1.0:
        wtd_avg = title
    elif descr > 1.0:
        wtd_avg = descr
    
    return wtd_avg
        
def check_correct (queries):
    attr_tally = [0] * 2    # [sum of diffs, count, avg diff]
    brand_tally = [0] * 2
    color_tally = [0] * 2
    descr_tally = [0] * 2
    title_tally = [0] * 2
    mean_tally = [0] * 2
    
    # f = open('bad_attr_scores.txt', 'w')
    
    for query in queries:
        if query['attributes_score'] > -1:
            attr_tally[0] += math.fabs(query['attributes_score'] - query['actual_score'])
            attr_tally[1] += 1
        if query['brand_score'] > -1:
            brand_tally[0] += math.fabs(query['brand_score'] - query['actual_score'])
            brand_tally[1] += 1
        if query['color_score'] > -1:
            color_tally[0] += math.fabs(query['color_score'] - query['actual_score'])
            color_tally[1] += 1
        # if query['color_score'] > 1.0 and query['color_score']<3.0:
            # print (query['color_score'], query['actual_score'], abs(query['color_score'] - query['actual_score']))
        if query['description_score'] > -1:
            descr_tally[0] += math.fabs(query['description_score'] - query['actual_score'])
            descr_tally[1] += 1
        if query['title_score'] > -1:
            title_tally[0] += math.fabs(query['title_score'] - query['actual_score'])
            title_tally[1] += 1
        if query['mean_score'] > -1:
            mean_tally[0] += math.fabs(query['mean_score'] - query['actual_score'])
            mean_tally[1] += 1
            
        # if abs(query['attributes_score'] - query['actual_score']) > 1.0:
            # f.write(str(int(100 * query['attributes_score'])/100) + ' ' + str(int(100 * query['actual_score'])/100) + str(query['keywords']) + ' | ' + products[query['prod_id']]['attributes'] + '\n')
    # f.close() 
    print ('Attributes: ', attr_tally[0] / attr_tally[1])
    print ('Brand: ', brand_tally[0] / brand_tally[1])
    print ('Color: ', color_tally[0] / color_tally[1])
    print ('Description: ', descr_tally[0] / descr_tally[1])
    print ('Title: ', title_tally[0] / title_tally[1])
    print ('Mean: ', mean_tally[0] / mean_tally[1])
    
def get_scores (products, queries, filename = None):
    output = '"id","relevance"\n'
    for query in queries:
        attrs_score = 0
        descr_score = 0
        title_score = 0
        brand_score = -100
        
        product = products[query['prod_id']]
        for kw in query['keywords']:
        
            if 'description' in product and kw in product['description'] and kw in word_counts:
                kw_count = product['description'].count(kw)
                idf = math.log(len(word_counts) / word_counts[kw][0])
                descr_score += min(3.8/len(query['keywords']), float(kw_count * 8.2 / idf))
                # descr_score += min(1.25, float(kw_count * 8.2 / idf))
                # print(float(kw_count * 1.2 / idf))
        
            # if 'attributes' in product and kw in product['attributes']:
                # attrs_score += 1.12
            # elif 'attributes' not in product:
                # attrs_score = -100
                
            # if 'description' in product and kw in product['description']:
                # kw_count = product['description'].count(kw)
                # tflts = kw_count * len(kw) ** 2.2     #term frequency x lenth of term squared
                # descr_score += min([1.33, 1.07 + (tflts - 39) / 370])
            # elif 'description' not in product:
                # descr_score = -100
                
            # if 'title' in product and kw in product['title']:
                # title_score += 1.13
            # elif 'title' not in product:
                # title_score = -100
                
            # if 'brand' in product and kw in product['brand']:
                # brand_score = 2.55
             
        # adjust down if greater than 3.0
        if attrs_score * 2.0 / len(query['keywords']) > 2.0:
            attrs_score = len(query['keywords'])
        if descr_score * 2.0 / len(query['keywords']) > 2.0:
            descr_score = len(query['keywords'])
        if title_score * 2.0 / len(query['keywords']) > 2.0:
            title_score = len(query['keywords'])
            
        query['attributes_score'] = attrs_score * 2.0 / len(query['keywords']) + 1
        query['description_score'] = descr_score * 2.0 / len(query['keywords']) + 1
        query['title_score'] = title_score * 2.0 / len(query['keywords']) + 1
        query['brand_score'] = brand_score
        query['mean_score'] = get_mean_score([query['attributes_score'], query['brand_score'], query['description_score'], query['title_score']])
        
        if filename:
            output += str(query['id']) + ',' + str(query['mean_score']) + '\n'
        
    if filename:
        with open (filename, 'w') as fout:
            fout.write(output)
            
    return queries
    
def get_ngram_scores (products, queries, filename = None):
    output = '"id","relevance"\n'
    for query in queries:
        attrs_score = 0
        brand_score = -100
        color_score = -100
        descr_score = 0
        title_score = 0
        n = 2   # num of characters per n-gram 
        
        product = products[query['prod_id']]
        query_string = ' '.join(query['keywords'])
        for i in range (len(query_string) - n):
            kw = query_string[i:i+n]
            if 'description' in product and kw in product['description']:
                descr_score += float(n*1.46/len(query_string))              
            if 'title' in product and kw in product['title']:
                title_score += float(n*1.83/len(query_string))
            
        for kw in query['keywords']:
            # if 'attributes' in product and kw in product['attributes']:
                # attrs_score += 1.12
            if 'attributes' in product and kw in product['attributes']:
                score = 0.9 + product['attributes'].count(kw) * 0.08
                attrs_score += min(1.4, score)
            elif 'attributes' not in product:
                attrs_score = -100
            if 'brand' in product and kw in product['brand']:
                if brand_score > 2.45:
                    brand_score = 2.6
                else:
                    brand_score = 2.5
            if 'color' in product and kw in product['color']:
                if color_score > 2.45:
                    color_score = 2.72
                else:
                    color_score = 2.53
                    
        # adjust down if greater than 3.0
        if attrs_score * 2.0 / len(query['keywords']) > 2.0:
            attrs_score = len(query['keywords'])
        if color_score * 2.0 / len(query['keywords']) > 2.0:
            color_score = len(query['keywords'])
        if descr_score * 2.0 / len(query['keywords']) > 2.0:
            descr_score = len(query['keywords'])
        if title_score * 2.0 / len(query['keywords']) > 2.0:
            title_score = len(query['keywords'])
            
        # scale scores to 1.0-3.0 range
        query['attributes_score'] = attrs_score * 2.0 / len(query['keywords']) + 1
        query['color_score'] = color_score * 2.0 / len(query['keywords']) + 1
        query['description_score'] = descr_score * 2.0 / len(query['keywords']) + 1
        query['title_score'] = title_score * 2.0 / len(query['keywords']) + 1
        query['brand_score'] = brand_score
        query['mean_score'] = get_mean_score([query['attributes_score'], query['brand_score'], query['description_score'], query['title_score'], query['color_score']])
                
        if filename:
            output += str(query['id']) + ',' + str(query['mean_score']) + '\n'
        
    if filename:
        with open (filename, 'w') as fout:
            fout.write(output)
            
    return queries
    
products = {}
word_counts = {}
print ('Loading Descriptions')
products, word_counts = load_descriptions('product_descriptions_clean.csv', products)
print ('Num products: ', len(products), ' Word count: ', len(word_counts))

print ('Loading Attributes')
products = load_attributes('attributes.csv', products)
print ('Num products: ', len(products))

#===============================================
print ('Loading Train Titles')
products = load_titles('train.csv', products)
print ('Num products: ', len(products))

print ('Loading Train Queries')
train_queries = load_queries('train.csv', products)
print ('Num queries: ', len(train_queries))

print ('Predicting Scores')
train_queries = get_ngram_scores (products, train_queries)

print ('Checking Correct')
check_correct (train_queries)
#================================================

# print ('Loading Test Titles')
# products = load_titles('test.csv', products)
# print ('Num products: ', len(products))

# print ('Loading Test Queries')
# test_queries = load_queries('test.csv', products, False)
# print ('Num test queries: ', len(test_queries))

# print ('Writing Scores')
# get_ngram_scores (products, test_queries, 'submission.csv')