# CS-223: Bioinformatics
# Dr. Wesley
# File Name:  SuperQuery.py 
# Project: Kaggle Home Depot Search Relevance
# By: Joe James, Aneri Chavda, Tejas Saoji, Lingfang Gao, Rachana Desai 
# Date: May 12, 2016 
# Python Version(s) 2.7.3: 
""" 
FILE DESCRIPTION: 
    The SuperQuery file contains the SuperQuery class, which is the parent class
    for Query.
    This file depends upon no other modules.

MODIFICATION HISTORY:     
    4/20/2016, created SuperQuery class, Joe James, Aneri Chavda, Tejas Saoji, Lingfang Gao, Rachana Desai 
"""
class SuperQuery:
    """ The SuperQuery class is the parent class for Query. SuperQuery really
    doesn't do anything other than being a super-class that gives Query something
    to inherit from. """
    
    def __init__(self):
        pass